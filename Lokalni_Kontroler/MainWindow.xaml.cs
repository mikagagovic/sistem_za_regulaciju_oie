﻿using System.Windows;
using Lokalni_Kontroler.ViewModel;
using System.ServiceModel;
using Common;
using System.ServiceModel.Description;

namespace Lokalni_Kontroler
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            MainWindowViewModel.dataGrid = dGrid;

            NetTcpBinding binding = new NetTcpBinding();
            string address = "net.tcp://localhost:9998/RES";
            ServiceHost host = new ServiceHost(typeof(MainWindowViewModel));
            host.AddServiceEndpoint(typeof(IKontrolToRes), binding, address);

            host.Description.Behaviors.Remove(typeof(ServiceDebugBehavior));
            host.Description.Behaviors.Add(new ServiceDebugBehavior() { IncludeExceptionDetailInFaults = true });

            host.Open();
        }
    }
}

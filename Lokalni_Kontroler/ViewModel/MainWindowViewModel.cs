﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using Common.Entities;
using System.Xml;
using System.Windows;
using System.ServiceModel;
using System.Windows.Input;
using System.Windows.Forms;
using System.Windows.Controls;
using System.Threading;
using System.Collections.ObjectModel;

namespace Lokalni_Kontroler.ViewModel
{
    public class MainWindowViewModel : INotifyPropertyChanged, IXmlCommunicate,IKontrolToRes
    {
        public static System.Windows.Controls.DataGrid dataGrid;
        private Object thisLock = new Object();
        private Object remoteLock = new object();
        public Thread procesThread;
        NetTcpBinding binding = new NetTcpBinding();
        string address = "net.tcp://localhost:9999/Kontroler";
        private ObservableCollection<Jedinica> list = new AsyncObservableCollection<Jedinica>();
        List<Jedinica> acceptBufferForResive = new List<Jedinica>();
        private ICommand openFileDialog;
        private ICommand createCommand;
        private ICommand posaljiCommand;
        private ICommand readcommand;
        private string filePath;
        private int id;
        private Tip tipJedinice;
        private double tas;
        private double min;
        private double max;
        private Kontrola kontrolaJed;
        private double cena;
        private static IXmlCommunicate viewvmodel;
        private static IKontrolToRes kontrolToRes;
        public static IXmlCommunicate Instance
        {
            get
            {
                if (Viewvmodel == null)
                {
                    Viewvmodel = new MainWindowViewModel();
                }
                return viewvmodel;
            }
            set
            {
                if (Viewvmodel == null)
                {
                    viewvmodel = value;
                }
            }
        }
        public static IKontrolToRes InstanceKontrol
        {
            get
            {
                if (KontrolToRes == null)
                {
                    KontrolToRes = new MainWindowViewModel();
                }
                return kontrolToRes;
            }
            set
            {
                if (KontrolToRes == null)
                {
                    kontrolToRes = value;
                }
            }
        }
        public static IXmlCommunicate Viewvmodel { get => viewvmodel; set => viewvmodel = value; }
        public static IKontrolToRes KontrolToRes { get => kontrolToRes; set => kontrolToRes = value; }

        public List<Jedinica> AcceptBufferForResive
        {
            get { return acceptBufferForResive; }
            set { acceptBufferForResive = value; }
        }
        public string FilePath
        {
            get { return filePath; }
            set
            {
                filePath = value;
                OnPropertyChanged("FilePath");

            }
        }


        public MainWindowViewModel()
        {
            procesThread = new Thread(() => Process());
            procesThread.Start();
        }
        #region properties
        public ICommand OpenFileDialog
        {
            get
            {
                return openFileDialog ?? (openFileDialog = new RelayCommand(param => this.OpenFile()));
            }
        }

        public ICommand NapraviCommand
        {
            get
            {
                return createCommand ?? (createCommand = new RelayCommand(param => this.Create()));
            }
        }
        public ICommand PosaljiCommand
        {
            get
            {
                return posaljiCommand ?? (posaljiCommand = new RelayCommand(param => this.SendRES()));
            }
        }

        public ICommand ReadFromFile
        {
            get
            {
                return readcommand ?? (readcommand = new RelayCommand(param => this.Read(FilePath)));
            }
        }

#endregion
        private void SendRES()
        {
            if (dataGrid.SelectedItem != null)
            {
                Jedinica j = (Jedinica)dataGrid.SelectedItem;
                ResWCF proxy = new ResWCF(binding, new EndpointAddress(address));
                bool res = proxy.Introduce(j);
                if (res == false)
                {
                    System.Windows.MessageBox.Show("Veza ka serveru nije uspostavljena");
                    return;
                }

                else
                {
                    if (j.Kontrola.Equals(Kontrola.Local))
                    {
                        Thread t = new Thread(() => SendData(j));
                        t.Start();
                    }
                }
            }
        }
        public void SendData (Jedinica j)
        {
            bool stopFlag = true;
            while (stopFlag)
            {

                Thread.Sleep(3000);
                Random r = new Random();
                double tSnaga = r.NextDouble() * (j.SnagaMax - j.SnagaMin) + j.SnagaMin;
                j.TAktivnaSnaga = Math.Round(tSnaga,2);
                //  bindingList[j.Kod].TAktivnaSnaga = tSnaga;
                 
                ResWCF proxy = new ResWCF(binding, new EndpointAddress(address));
                bool result = proxy.SendInformation(j);
                if (result == false)
                {
                    stopFlag = false;
                }

            }
        }

        private void Create()
        {
            Jedinica j = new Jedinica(id, TipJedinice, tas, min, max, KontrolaJed, cena);
            Id += 1;
            //bindingList.Add(j);

            if (FilePath != null && FilePath != "")
            {
                Write(filePath);
            }
            System.Windows.MessageBox.Show("Generator je kreiran");


        }

        public int Id { get => id; set => id = value; }
        public Tip TipJedinice { get => tipJedinice; set => tipJedinice = value; }
        public double Tas { get => tas; set => tas = value; }
        public double Min { get => min; set => min = value; }
        public double Max { get => max; set => max = value; }
        public Kontrola KontrolaJed { get => kontrolaJed; set => kontrolaJed = value; }
        public double Cena { get => cena; set => cena = value; }
        public ObservableCollection<Jedinica> bindingList
        {
            get { return list; }
            set { list = value; }
        }

        public bool Write(string path)
        {
            lock (thisLock)
            {

                XmlDocument doc = new XmlDocument();
                try
                {
                    doc.Load(path);
                }
                catch (Exception)
                {

                    return false;
                }

                XmlNode element = doc.CreateElement("Element");
                XmlNode idEl = doc.CreateElement("id");
                idEl.InnerText = Id.ToString();

                XmlNode tip = doc.CreateElement("tip");
                tip.InnerText = TipJedinice.ToString();

                XmlNode aktivna = doc.CreateElement("trenutna_A_S");
                aktivna.InnerText = Tas.ToString();

                XmlNode min = doc.CreateElement("min");
                min.InnerText = Min.ToString();

                XmlNode max = doc.CreateElement("max");
                max.InnerText = Max.ToString();

                XmlNode kontrola = doc.CreateElement("kontrola");
                kontrola.InnerText = KontrolaJed.ToString();

                XmlNode cena = doc.CreateElement("cena");
                cena.InnerText = Cena.ToString();


                element.AppendChild(idEl); element.AppendChild(tip);
                element.AppendChild(aktivna); element.AppendChild(min);
                element.AppendChild(max); element.AppendChild(kontrola);
                element.AppendChild(cena);


                doc.DocumentElement.AppendChild(element);
                try
                {
                    doc.Save(path);
                }
                catch (Exception)
                {
                    return false;
                }
            }

            return true;
        }

        public bool Read(string path)
        {
            bindingList.Clear();
          
                try
                {
                    XmlDocument doc = new XmlDocument();
                    try
                    {
                        doc.Load(path);
                    }
                    catch (Exception e)
                    {
                        System.Windows.MessageBox.Show(e.ToString());
                    }

                    //string ReadResult = "";
                    XmlNodeList elemList = doc.GetElementsByTagName("Element");
                    foreach (XmlNode node in elemList)
                    {
                        Jedinica j = new Jedinica();
                        j.Kod = int.Parse(node.FirstChild.InnerText);
                        j.TipJedinice = (Tip)Enum.Parse(typeof(Tip),node.FirstChild.NextSibling.InnerText);
                        j.TAktivnaSnaga =double.Parse( node.FirstChild.NextSibling.NextSibling.InnerText);
                        j.SnagaMin = double.Parse(node.FirstChild.NextSibling.NextSibling.NextSibling.InnerText);
                        j.SnagaMax = double.Parse(node.FirstChild.NextSibling.NextSibling.NextSibling.NextSibling.InnerText);
                        j.Kontrola = (Kontrola)Enum.Parse(typeof(Kontrola),node.LastChild.PreviousSibling.InnerText);
                        j.Cena = double.Parse(node.LastChild.InnerText);
                        bindingList.Add(j);
                      
                    }
                    return true;

                }
                catch (Exception)
                {

                    return false;
                }
        }
        private void OpenFile()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Open xml file..";
            openFileDialog.Filter = "XML Files|*.xml;|All Files|*.*";
            openFileDialog.RestoreDirectory = true;

            DialogResult dialogResponse = openFileDialog.ShowDialog();
            if (dialogResponse == DialogResult.OK)
            {
                FilePath = openFileDialog.FileName;
                //  myStream = openFileDialog.OpenFile();

            }
        }

        public bool SendInformationFromKontroler(Jedinica j)
        {
            if (j != null)
            {
                lock (remoteLock)
                {
                    if (j.Kontrola == Kontrola.Remote)
                        AcceptBufferForResive.Add(j);
                }
                return true;
            }
            else
                return false;

        }

        public void Process()
        {
            while (true)
            {
                Thread.Sleep(10000);
                lock (remoteLock)
                {
                    if (bindingList.Count != 0)
                    {
                        foreach (var jedinica in bindingList)
                        {
                            try
                            {
                                if (jedinica.Kod == acceptBufferForResive.FirstOrDefault(x => x.Kod == jedinica.Kod).Kod)
                                {
                                    Jedinica j = acceptBufferForResive.FirstOrDefault(x => x.Kod == jedinica.Kod);
                                    jedinica.TAktivnaSnaga = j.TAktivnaSnaga;
                                    acceptBufferForResive.Remove(j);
                                }
                            }
                            catch (Exception)
                            {
                                // MessageBox.Show("Exception");
                                continue;
                            }
                        }
                    }
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

       
    }
}

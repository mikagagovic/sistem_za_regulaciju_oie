﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Common;
using Common.Entities;

namespace Lokalni_Kontroler.ViewModel
{
   public class ResWCF: ChannelFactory<IResToKontrolContract>, IResToKontrolContract, IDisposable
    {
        private IResToKontrolContract _factory;

        public bool Introduce(Jedinica j)
        {
            bool res;
            try
            {
                _factory.Introduce(j);
                res = true;
                return res;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error while trying to read. {0}", e.Message);
                res = false;
                return res;
            }
        }

        public bool RecieveInformatin(Jedinica j)
        {
            bool res;
            try
            {
                _factory.RecieveInformatin(j);
                res = true;
                return res;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error while trying to read. {0}", e.Message);
                res = false;
                return res;
            }
        }

        public bool SendInformation(Jedinica j)
        {
            bool res;
            try
            {
                _factory.SendInformation(j);
                res = true;
                return res;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error while trying to read. {0}", e.Message);
                res = false;
                return res;
            }
        }

        public ResWCF(NetTcpBinding binding, EndpointAddress address)
            : base(binding, address)
        {
            _factory = CreateChannel();
        }

        void IDisposable.Dispose()
        {
            if (_factory != null)
                _factory = null;

            Close();
        }
    
    }
}

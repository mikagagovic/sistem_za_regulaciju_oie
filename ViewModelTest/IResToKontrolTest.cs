﻿using Common;
using Common.Entities;
using NSubstitute;
using NUnit.Framework;
using Sistem_za_regulaciju_oie.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModelTest
{
    [TestFixture]
    public class IResToKontrolTest
    {
        private MainWindowViewModel viewModel;
        [OneTimeSetUp]
        public void SetupTest()
        {
            Jedinica j = new Jedinica();
            viewModel = new MainWindowViewModel();
            MainWindowViewModel.Instance = Substitute.For<IResToKontrolContract>();
            MainWindowViewModel.Instance.Introduce(j).Returns(true);
            MainWindowViewModel.Instance.Introduce(null).Returns(false);
            MainWindowViewModel.Instance.SendInformation(j).Returns(true);
            MainWindowViewModel.Instance.SendInformation(null).Returns(false);



        }
        [Test]
        public void IntroduceTrue()
        {
            Jedinica j = new Jedinica();
            bool result = viewModel.Introduce(j);
            Assert.IsTrue(result);
        }
        [Test]
        public void IntroduceFalse()
        {
            bool result = viewModel.Introduce(null);
            Assert.IsFalse(result);
        }
        [Test]
        public void SendInformationTrue()
        {
            Jedinica j = new Jedinica();
            bool result = viewModel.SendInformation(j);
            Assert.IsTrue(result);
        }
        [Test]
        public void SendInformationFalse()
        {
            bool result = viewModel.SendInformation(null);
            Assert.IsFalse(result);
        }


    }
}

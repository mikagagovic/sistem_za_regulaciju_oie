﻿using Common;
using Common.Entities;
using Lokalni_Kontroler.ViewModel;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModelTest
{
    [TestFixture]
    public class IKontrolToResTest
    {
        private MainWindowViewModel viewModel;

        [OneTimeSetUp]
        public void SetupTest()
        {
            viewModel = new MainWindowViewModel();
            MainWindowViewModel.KontrolToRes = Substitute.For<IKontrolToRes>();
            Jedinica j = new Jedinica();
            MainWindowViewModel.KontrolToRes.SendInformationFromKontroler(j).Returns(true);
            MainWindowViewModel.KontrolToRes.SendInformationFromKontroler(null).Returns(false);


        }
        [Test]
        public void SendInformationFromKontrolerTrue()
        {
            Jedinica j = new Jedinica();
            bool result = viewModel.SendInformationFromKontroler(j);
            Assert.IsTrue(result);
        }
        [Test]
        public void SendInformationFromKontrolerFalse()
        {
            bool result = viewModel.SendInformationFromKontroler(null);
            Assert.IsFalse(result);
        }

        //public void WriteTestTrue()
        //{
        //    bool result = viewModel.Write(path);
        //    Assert.IsFalse(result);
        //}

        //[Test]
        //public void WriteTestFalse()
        //{
        //    bool result = viewModel.Write(path + "pa");
        //    Assert.IsFalse(result);
        //}
    }
}

﻿using Common.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModelTest.EntitiesTest
{
    [TestFixture]
    public class GrupaTest
    {
        private Grupa grupa;

        [OneTimeSetUp]
        public void SetupTest()
        {
            grupa = new Grupa();
        }
        [Test]
        public void ConstructorTest()
        {
            Assert.DoesNotThrow(() => new Grupa());
        }


        [Test]
        public void KodPropertyTest()
        {
            int id = 100;
            grupa.KodGrupe = id;
            Assert.AreEqual(id, grupa.KodGrupe);
        }

       

        [Test]
        public void BrojJedinicaPropertyTest()
        {
            int brojJedinica = 32;
            grupa.BrojJedinica = brojJedinica;
            Assert.AreEqual(brojJedinica, grupa.BrojJedinica);
        }

        [Test]
        public void TrenutnaProizvodnjaPropertyTest()
        {
            double trenutna = 32;
            grupa.TrenutnaProizvodnja = trenutna;
            Assert.AreEqual(trenutna, grupa.TrenutnaProizvodnja);
        }
        [Test]
        public void MaxPropertyTest()
        {
            double max = 32;
            grupa.MaxProizvodnja = max;
            Assert.AreEqual(max, grupa.MaxProizvodnja);
        }

    }
}

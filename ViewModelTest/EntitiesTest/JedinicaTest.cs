﻿using Common.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModelTest.EntitiesTest
{
    [TestFixture]
    public class JedinicaTest
    {
        private Jedinica jedinica;

        [OneTimeSetUp]
        public void SetupTest()
        {
            jedinica = new Jedinica();
        }
        [Test]
        public void ConstructorTest()
        {
            Assert.DoesNotThrow(() => new Jedinica());
        }

        [Test]
        public void ConstructorParamTest()
        {
            int kod = 1;
            Tip tipJedinice = Tip.micro_hydro;
            double tAktivnaSnaga = 32;
            double snagaMin = 20;
            double snagaMax = 1100;
            Kontrola kontrola = Kontrola.Local;
            double cena = 43;
            Assert.DoesNotThrow(() => new Jedinica(kod, tipJedinice, tAktivnaSnaga, snagaMin, snagaMax, kontrola, cena));

        }


        [Test]
        public void KodPropertyTest()
        {
            int id = 100;
            jedinica.Kod = id;
            Assert.AreEqual(id, jedinica.Kod);
        }

        [Test]
        public void TipPropertyTest()
        {
            Tip tipJedinice = Tip.micro_hydro;
            jedinica.TipJedinice = tipJedinice;
            Assert.AreEqual(tipJedinice, jedinica.TipJedinice);

        }

        [Test]
        public void TAktivnaPropertyTest()
        {
            double tAktivnaSnaga = 32;
            jedinica.TAktivnaSnaga= tAktivnaSnaga;
            Assert.AreEqual(tAktivnaSnaga, jedinica.TAktivnaSnaga);
        }

        [Test]
        public void MinPropertyTest()
        {
            double min = 32;
            jedinica.SnagaMin = min;
            Assert.AreEqual(min, jedinica.SnagaMin);
        }
        [Test]
        public void MaxPropertyTest()
        {
            double max = 32;
            jedinica.SnagaMax = max;
            Assert.AreEqual(max, jedinica.SnagaMax);
        }

        [Test]
        public void KontrolaPropertyTest()
        {
            Kontrola kontrola = Kontrola.Local;
            jedinica.Kontrola = kontrola;
            Assert.AreEqual(kontrola, jedinica.Kontrola);

        }
        [Test]
        public void CenaPropertyTest()
        {
            double cena = 32;
            jedinica.Cena = cena;
            Assert.AreEqual(cena, jedinica.Cena);
        }


    }
}

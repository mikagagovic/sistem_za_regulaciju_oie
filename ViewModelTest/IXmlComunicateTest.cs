﻿using Common;
using Lokalni_Kontroler.ViewModel;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModelTest
{
    [TestFixture]
    public class IXmlComunicateTest
    {
        private MainWindowViewModel viewModel;
        string path = (System.IO.Path.GetDirectoryName(
             System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\testData.xml").Substring(8);

        [OneTimeSetUp]
        public void SetupTest()
        {
            viewModel = new MainWindowViewModel();
            MainWindowViewModel.Instance = Substitute.For<IXmlCommunicate>();

            MainWindowViewModel.Instance.Read(path).Returns(true);
            MainWindowViewModel.Instance.Read(path + "path").Returns(false);
            MainWindowViewModel.Instance.Write( path).Returns(true);
            MainWindowViewModel.Instance.Write( path + "pa").Returns(false);
        }
        [Test]
        public void ReadTestTrue()
        {
            bool result = viewModel.Read(path);
            Assert.IsTrue(result);
        }
       
        public void WriteTestTrue()
        {
            bool result = viewModel.Write( path);
            Assert.IsFalse(result);
        }

        [Test]
        public void WriteTestFalse()
        {
            bool result = viewModel.Write( path + "pa");
            Assert.IsFalse(result);
        }

       
    }
}

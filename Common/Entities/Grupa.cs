﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class Grupa
    {
        private int kodGrupe;
        private int brojJedinica;
        private double trenutnaProizvodnja;
        private double maxProizvodnja;

        public Grupa ()
        {

        }

        public int KodGrupe { get => kodGrupe; set => kodGrupe = value; }
        public int BrojJedinica { get => brojJedinica; set => brojJedinica = value; }
        public double TrenutnaProizvodnja { get => trenutnaProizvodnja; set => trenutnaProizvodnja = value; }
        public double MaxProizvodnja { get => maxProizvodnja; set => maxProizvodnja = value; }
    }
}

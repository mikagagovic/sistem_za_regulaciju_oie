﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public enum Tip
    {
        solar= 0,
        wind,
        micro_hydro
    }

    public enum Kontrola
    {
        Local = 0,
        Remote
    }
    public class Jedinica:INotifyPropertyChanged
    {
        private int kod;
        private Tip tipJedinice;
        private double tAktivnaSnaga;
        private double snagaMin;
        private double snagaMax;
        private Kontrola kontrola;
        private double cena;

        public Jedinica()
        {

        }

        public Jedinica(int kod, Tip tipJedinice, double tAktivnaSnaga, double snagaMin, double snagaMax, Kontrola kontrola, double cena)
        {
            this.kod = kod;
            this.tipJedinice = tipJedinice;
            this.tAktivnaSnaga = tAktivnaSnaga;
            this.snagaMin = snagaMin;
            this.snagaMax = snagaMax;
            this.kontrola = kontrola;
            this.cena = cena;
        }

        public int Kod { get => kod; set => kod = value; }
        public Tip TipJedinice { get => tipJedinice; set => tipJedinice = value; }
        public double TAktivnaSnaga
        {
            get
            {
                return tAktivnaSnaga;
            }
            set
            {
                tAktivnaSnaga = value;
                OnPropertyChanged("TAktivnaSnaga");
            }
        }
        public double SnagaMin { get => snagaMin; set => snagaMin = value; }
        public double SnagaMax { get => snagaMax; set => snagaMax = value; }
        public double Cena { get => cena; set => cena = value; }
        public Kontrola Kontrola { get => kontrola; set => kontrola = value; }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public interface IXmlCommunicate
    {
        bool Write(string path);
        bool Read(string path);
    }
}

﻿using Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace Common
{
    [ServiceContract]
   public interface IResToKontrolContract
    {
        [OperationContract]
        bool Introduce(Jedinica j);
        [OperationContract]
        bool SendInformation(Jedinica j);
        [OperationContract]
        bool RecieveInformatin(Jedinica j);
    }
}

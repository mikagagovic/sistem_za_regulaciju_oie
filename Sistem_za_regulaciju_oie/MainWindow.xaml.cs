﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ServiceModel;
using System.ServiceModel.Description;
using Common;
using Sistem_za_regulaciju_oie.ViewModel;

namespace Sistem_za_regulaciju_oie
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            MainWindowViewModel.tbPower = tbPower;
            NetTcpBinding binding = new NetTcpBinding();
            string address = "net.tcp://localhost:9999/Kontroler";
            ServiceHost host = new ServiceHost(typeof(MainWindowViewModel));
            host.AddServiceEndpoint(typeof(IResToKontrolContract), binding, address);
           
            host.Description.Behaviors.Remove(typeof(ServiceDebugBehavior));
            host.Description.Behaviors.Add(new ServiceDebugBehavior() { IncludeExceptionDetailInFaults = true });
            
            host.Open();
        }
    }
}

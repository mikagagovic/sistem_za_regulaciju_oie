﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Common.Entities;

namespace Sistem_za_regulaciju_oie.ViewModel
{
    public class KontrolerWCF:ChannelFactory<IKontrolToRes>, IKontrolToRes, IDisposable
    {
        private IKontrolToRes _factory;

        public KontrolerWCF(NetTcpBinding binding, EndpointAddress address)
            : base(binding, address)
        {
            _factory = CreateChannel();
        }

        public bool SendInformationFromKontroler(Jedinica j)
        {
            bool res;
            try
            {
                _factory.SendInformationFromKontroler(j);
                res = true;
                return res;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error while trying to read. {0}", e.Message);
                res = false;
                return res;
            }
        }
    }
}

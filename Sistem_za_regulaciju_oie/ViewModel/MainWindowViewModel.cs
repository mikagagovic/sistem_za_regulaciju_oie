﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using Common.Entities;
using System.Threading;
using System.Windows;
using System.Collections.ObjectModel;
using System.ServiceModel;
using System.Windows.Input;
using System.Windows.Controls;

namespace Sistem_za_regulaciju_oie.ViewModel
{
    public class MainWindowViewModel : INotifyPropertyChanged,IResToKontrolContract
    {
        private static ObservableCollection<Jedinica> list = new AsyncObservableCollection<Jedinica>();
        private static ObservableCollection<Jedinica> listRemote = new AsyncObservableCollection<Jedinica>();
        public Thread procesThread;
        public Object lockObj = new object();
        public Object lockRemote = new object();
        List<Jedinica> acceptBuffer = new List<Jedinica>();
        NetTcpBinding binding = new NetTcpBinding();
        string address = "net.tcp://localhost:9998/RES";
        private Object remoteLock = new Object();
        public static TextBox tbPower;
        private static IResToKontrolContract viewvmodel;

        public static IResToKontrolContract Instance
        {
            get
            {
                if (Viewvmodel == null)
                {
                    Viewvmodel = new MainWindowViewModel();
                }
                return Viewvmodel;
            }
            set
            {
                if (Viewvmodel == null)
                {
                    Viewvmodel = value;
                }
            }
        }
        public static IResToKontrolContract Viewvmodel { get => viewvmodel; set => viewvmodel = value; }


        private ICommand sendRemoteCommand;
        public MainWindowViewModel()
        {
            procesThread = new Thread(() => Process());
            procesThread.Start();
            
        }
        public ObservableCollection<Jedinica> bindList
        {
            get { return list; }
            set { list = value; }
        }
        public ObservableCollection<Jedinica> bindListRemote
        {
            get { return listRemote; }
            set { listRemote = value; }
        }

        public ICommand SendRemoteCommand
        {
            get
            {
                return sendRemoteCommand ?? (sendRemoteCommand = new RelayCommand(param => this.SendData(tbPower.Text)));
            }
        }

       

        private void SendData(string wantedpower )
        {
            Thread t = new Thread(() => SendDataThread(wantedpower));
            t.Start();
        }

        private void SendDataThread(string wantedpower) // proracun setpointa i slanje na RES
        {
            bool stopFlag = true;
            while (stopFlag)
            {
                Thread.Sleep(10000);
                lock (lockRemote)
                {

                    if (bindListRemote.Count > 0)
                    {
                        double powerfd = 0;
                        if (wantedpower != "")
                        {
                           powerfd = double.Parse(wantedpower);
                        }
                        bindListRemote.OrderBy(x => x.Cena);
                        for (int i = 0; i < bindListRemote.Count; i++)
                        {
                            bindList[i].TAktivnaSnaga = (0.5 - 0.1 * i) * powerfd;

                            KontrolerWCF proxy = new KontrolerWCF(binding, new EndpointAddress(address));
                            bool result = proxy.SendInformationFromKontroler((Jedinica)bindList[i]);
                            if (result == false)
                            {
                                stopFlag = false;
                            }
                        }
                    }
                }
            }
        }


        protected virtual void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        public  void Process()
        {
            while(true)
            {
                Thread.Sleep(10000);
                lock (lockObj)
                {
                    if (bindList.Count != 0)
                    {
                        foreach (var jedinica in bindList)
                        {
                            try
                            {
                                if (jedinica.Kod == acceptBuffer.FirstOrDefault(x => x.Kod == jedinica.Kod).Kod)
                                {
                                    Jedinica j = acceptBuffer.FirstOrDefault(x => x.Kod == jedinica.Kod);
                                    jedinica.TAktivnaSnaga = j.TAktivnaSnaga;
                                    acceptBuffer.Remove(j);
                                }
                            }
                            catch (Exception)
                            {
                               // MessageBox.Show("Exception");
                                continue;
                            }
                            
                        }
                    }
                }
            }
        }

        public bool Introduce(Jedinica j)
        {
            if (j != null)
            {
                if (j.Kontrola == Kontrola.Remote)
                {
                    bindListRemote.Add(j);
                }
                bindList.Add(j);
                return true;
            }
            else return false;
        }

       

        public bool SendInformation(Jedinica j) // primio informacije od generatora u lakalu
        {
            if (j != null)
            {
                lock (lockObj)
                {
                    if (j.Kontrola == Kontrola.Local)
                    acceptBuffer.Add(j);
                }
                return true;
            }
            else
                return false;
        }

        

        public bool RecieveInformatin(Jedinica j)
        {
            throw new NotImplementedException();
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
